# Hausaufgabe 3

## Aufgabe 3 - Quaderberechnungen (10 Punkte)

Schreiben Sie ein C-Programm, das die Koordinaten der zwei Eckpunkte vom Bildschirm
einliest sowie folgende Maßzahlen berechnet und am Bildschirm ausgibt:

* Volumen
* Oberfläche
* Gesamtlänge aller Kanten

Für die Berechnungen sollen ein eigenes C-Modul mit passenden, sinnvollen Routinen
erstellt werden. Kommentieren Sie das Modul und die Routinen.
