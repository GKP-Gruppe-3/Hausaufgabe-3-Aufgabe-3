#include "stdafx.h"

#include "cuboid.h"

// Einen Quader anhand zweier Punkte ermitteln, wobei angenommen wird,
// dass der Quader an den Achsen ausgerichtet ist.
cuboid_t getCuboidBy2Points(point_t *point1, point_t *point2)
{
  cuboid_t cuboid;

  // Annehmen, dass der Quader an den Achsen ausgerichtet sein soll.
  // Die Kanten des Quaders aus den zwei Punkten berechnen.
  cuboid.a = fabs(point1->x - point2->x);
  cuboid.b = fabs(point1->y - point2->y);
  cuboid.c = fabs(point1->z - point2->z);

  return cuboid;
}

// Einen Quader auf der Konsole ausgeben.
void putCuboid(char *title, cuboid_t *cuboid)
{
  printf("%s:\n", title);
  printf("    a = %.2lf\n", cuboid->a);
  printf("    b = %.2lf\n", cuboid->b);
  printf("    c = %.2lf\n", cuboid->c);
}

// Das Volumen eines Quaders liefern.
double cuboidGetVolume(cuboid_t *cuboid)
{
  return cuboid->a * cuboid->b * cuboid->c;
}

// Den Flächeninhalt der Oberfläche eines Quaders liefern.
double cuboidGetSurfaceArea(cuboid_t *cuboid)
{
  return
    (2 * cuboid->a * cuboid->b) +
    (2 * cuboid->a * cuboid->c) +
    (2 * cuboid->b * cuboid->c);
}

// Den Gesamtlänge der Kanten eines Quaders liefern.
double cuboidGetTotalEdgeLength(cuboid_t *cuboid)
{
  return
    (4 * cuboid->a) +
    (4 * cuboid->b) +
    (4 * cuboid->c);
}
