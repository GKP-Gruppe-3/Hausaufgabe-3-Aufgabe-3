#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

#include "point.h"
#include "cuboid.h"

// Die Konsole pausieren und anschließend reinigen.
void pause()
{
  _getch();
  system("cls");
}

/* Testwerte (mit Zeilenumbrüchen auf die Konsole kopieren):
1.9
8.2
3.7
4.6
15.26
14.87
*/
// Das Programm zur Aufgabe 3
void exercise(void)
{
  printf("Ein Quader soll ueber zwei Punkte definiert werden.\n");
  printf("Es wird angenommen, dass der Quader an den Achsen ausgerichtet ist.\n");

  point_t point1 = consoleGetPoint("Bitte den ersten Punkt eingeben (in cm)...\n");
  point_t point2 = consoleGetPoint("Bitte den zweiten Punkt eingeben (in cm)...\n");
  cuboid_t cuboid = getCuboidBy2Points(&point1, &point2);

  printf("\n");

  putPoint("Punkt 1 (in cm)", &point1);
  putPoint("Punkt 2 (in cm)", &point2);
  putCuboid("Quader (in cm)", &cuboid);

  double volume = cuboidGetVolume(&cuboid);
  double surfaceArea = cuboidGetSurfaceArea(&cuboid);
  double totalEdgeLength = cuboidGetTotalEdgeLength(&cuboid);
  printf("Volumen (in cm^3)           = %.2lf\n", volume);
  printf("Oberflaeche (in cm^2)       = %.2lf\n", surfaceArea);
  printf("Gesamtkantenlaenge (in cm)  = %.2lf\n", totalEdgeLength);
}

int main(void)
{
  exercise();
  pause();

  return 0;
}
