#ifndef __POINT_H__
#define __POINT_H__

#include <stdio.h>

struct point
{
  double x;
  double y;
  double z;
};

// Ein Punkt im dreidimensionalen Raum.
typedef struct point point_t;

// Einen "double"-Wert von der Konsole lesen.
double consoleGetDouble(char *title);

// Einen Punkt (3D) von der Konsole lesen.
point_t consoleGetPoint(char *title);

// Einen Punkt (3D) auf der Konsole ausgeben.
void putPoint(char *title, point_t *point);

#endif
